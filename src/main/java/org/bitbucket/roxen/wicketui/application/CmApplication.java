package org.bitbucket.roxen.wicketui.application;

import javax.enterprise.inject.spi.BeanManager;

import org.apache.wicket.cdi.CdiConfiguration;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.protocol.http.WebApplication;
import org.jboss.weld.environment.servlet.Listener;

import de.agilecoders.wicket.less.BootstrapLess;

public abstract class CmApplication extends WebApplication {

    @Override
    public void init() {
        super.init();
        getDebugSettings().setAjaxDebugModeEnabled(false);
        getMarkupSettings().setStripWicketTags(true);

        BootstrapLess.install(this);

        BeanManager manager = (BeanManager) getServletContext().getAttribute(Listener.BEAN_MANAGER_ATTRIBUTE_NAME);
        new CdiConfiguration(manager).configure(this);

        SecurePackageResourceGuard guard = new SecurePackageResourceGuard();
        guard.addPattern("+*.css");
        guard.addPattern("+*.less");
        guard.addPattern("+*.otf");
        guard.addPattern("+*.woff");
        guard.addPattern("+*.ttf");
        getResourceSettings().setPackageResourceGuard(guard);
    }
}
