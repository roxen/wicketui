package org.bitbucket.roxen.wicketui.panel.nodetree;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.markup.html.repeater.tree.NestedTree;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;
import org.bitbucket.roxen.wicketui.panel.nodelink.NodeLink;

@SuppressWarnings("serial")
public class NodeTreePanel extends Panel {

    private static final Logger LOG = Logger.getLogger(NodeTreePanel.class.getName());

    @Inject
    private Session session;

    public NodeTreePanel(String id, String rootPath, final ContentEditor contentEditor) {
        super(id);

        final NodeProvider nodeProvider = new NodeProvider(rootPath);
        NestedTree<String> tree = new NestedTree<String>("tree", nodeProvider) {

            @Override
            public Component newNodeComponent(String id, IModel<String> model) {
                return new org.apache.wicket.extensions.markup.html.repeater.tree.Node<String>(id, this, model) {

                    @Override
                    protected Component createContent(String id, IModel<String> model) {
                        return newContentComponent(id, model);
                    }

                    @Override
                    protected String getExpandedStyleClass(String t) {
                        return "node-icon icon-play icon-rotate-90";
                    }

                    @Override
                    protected String getCollapsedStyleClass() {
                        return "node-icon icon-play";
                    }
                };
            }

            @Override
            protected Component newContentComponent(String id, IModel<String> model) {
                try {
                    Node node = session.getNode(model.getObject());
                    String nodeTitle;
                    if (node.hasProperty("title")) {
                        nodeTitle = node.getProperty("title").getString();
                    } else {
                        nodeTitle = node.getName();
                    }
                    AjaxSubmitLink openNodeLink = contentEditor.createOpenNodeLink("link", node.getPath());

                    openNodeLink.setBody(Model.of(nodeTitle));
                    return new NodeLink(id, Model.of(node.getPath()), openNodeLink);
                } catch (ItemNotFoundException e) {
                    LOG.log(Level.WARNING, "Error creating component", e);
                } catch (RepositoryException e) {
                    LOG.log(Level.WARNING, "Error creating component", e);
                }

                return new Label(id, "[node not found]");
            }

            @Override
            protected boolean getStatelessHint() {
                return false;
            }
        };

        add(tree);
        tree.expand(rootPath);
    }
}
