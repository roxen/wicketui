package org.bitbucket.roxen.wicketui.panel.contenteditor;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class ContentEditorTabTitlePanel extends Panel {

    private static final int MAX_TITLE_LENGTH = 20;

    public ContentEditorTabTitlePanel(String id, ContentEditorTab tab, String title, ContentEditor contentEditor) {
        super(id);
        String tabTitle = title;

        if (tabTitle.length() > MAX_TITLE_LENGTH) {
            tabTitle = tabTitle.substring(0, MAX_TITLE_LENGTH).trim() + "...";
        }

        Panel panel = tab.getPanel();
        if (panel instanceof ContentEditorTabPanel) {
            ContentEditorTabPanel contentEditorTabPanel = (ContentEditorTabPanel) panel;
            if (contentEditorTabPanel.isEditState()) {
                tabTitle = "* " + tabTitle;
            }
        }

        Label titleLabel = new Label("title", tabTitle);
        titleLabel.add(new AttributeAppender("title", Model.of(title)));
        add(titleLabel);
        if (tab.isClosable()) {
            add(new ContentEditorCloseLink("close", tab.getTabId(), contentEditor));
        } else {
            add(new EmptyPanel("close"));
        }
    }
}
