package org.bitbucket.roxen.wicketui.panel.iptctree;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.markup.html.repeater.tree.NestedTree;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public class IptcTree extends Panel {

    public IptcTree(String id) {
        super(id);

        final IptcNodeProvider instance = IptcNodeProvider.getInstance();
        NestedTree<IptcNode> tree = new NestedTree<IptcNode>("tree", instance) {

            @Override
            protected Component newContentComponent(String id, IModel<IptcNode> model) {
                return new Label(id, model.getObject().getName());
            }

            @Override
            protected boolean getStatelessHint() {
                return false;
            }
        };
        add(tree);
    }
}
