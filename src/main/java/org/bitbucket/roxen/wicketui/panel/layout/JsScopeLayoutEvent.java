package org.bitbucket.roxen.wicketui.panel.layout;

import org.odlabs.wiquery.core.javascript.JsScope;
import org.odlabs.wiquery.core.javascript.JsScopeContext;

@SuppressWarnings("serial")
public abstract class JsScopeLayoutEvent extends JsScope {

    public JsScopeLayoutEvent() {
        super("paneName", "paneElement", "paneState", "paneOptions", "layoutName");
    }

    public static JsScopeLayoutEvent quickScope(final CharSequence javascriptCode) {
        return new JsScopeLayoutEvent() {

            @Override
            protected void execute(JsScopeContext scopeContext) {
                scopeContext.append(javascriptCode);
            }
        };
    }
}
