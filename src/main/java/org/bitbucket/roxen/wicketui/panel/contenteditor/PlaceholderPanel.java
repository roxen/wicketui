package org.bitbucket.roxen.wicketui.panel.contenteditor;

import org.apache.wicket.markup.html.panel.Panel;

@SuppressWarnings("serial")
public class PlaceholderPanel extends Panel {

    public PlaceholderPanel(String id) {
        super(id);
    }
}
