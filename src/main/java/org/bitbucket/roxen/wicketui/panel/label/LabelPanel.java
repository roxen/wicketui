package org.bitbucket.roxen.wicketui.panel.label;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

@SuppressWarnings("serial")
public class LabelPanel extends Panel {

    private Label label;

    public LabelPanel(String id, String text) {
        super(id);
        label = new Label("label", text);
        add(label);
    }

    public Label getLabel() {
        return label;
    }
}
