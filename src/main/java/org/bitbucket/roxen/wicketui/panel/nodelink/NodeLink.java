package org.bitbucket.roxen.wicketui.panel.nodelink;

import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class NodeLink extends Panel {

    public NodeLink(String id, Model<String> model, AjaxSubmitLink link) {
        super(id, model);
        add(link);
    }
}
