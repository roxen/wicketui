(function(CM) {
    CM.resizeTabs = function(tabbedPanelId) {
        var tabbedPanel = $(tabbedPanelId);
        var tabRow = tabbedPanel.find('.tab-row');
        var tabPanel = tabbedPanel.find('.tab-panel');
        var container = tabbedPanel.closest('.fixed-tabs-container');
        if (container.size() == 0) {
            alert('Need to specify a container element with class fixed-tabs-container to get fixed tabs.');
            return;
        }
        var tabRowHeight = tabRow.outerHeight();
        var height = container.height() - tabRowHeight;
        tabPanel.css('position', 'absolute').css('overflow', 'auto').css('top',
                tabRowHeight).css('width', '100%').outerHeight(height);
    };
    CM.initTabs = function(tabbedPanelId) {
        var tabbedPanel = $(tabbedPanelId);
        var paneElem = tabbedPanel.closest('.ui-layout-pane');
        if (paneElem.size() == 0) {
            setTimeout(function() {
                paneElem = tabbedPanel.closest('.ui-layout-pane');
                var pane = paneElem.data('layoutPane');
                pane.options.onresize = function() {
                    CM.resizeTabs(tabbedPanelId);
                };
                CM.resizeTabs(tabbedPanelId);
            }, 1);
        }
    };
}(window.CM = window.CM || {}));
