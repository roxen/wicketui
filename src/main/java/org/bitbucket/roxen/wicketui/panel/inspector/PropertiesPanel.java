package org.bitbucket.roxen.wicketui.panel.inspector;

import java.util.ArrayList;

import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.IMarkupFragment;
import org.apache.wicket.markup.Markup;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel;

@SuppressWarnings("serial")
public class PropertiesPanel extends Panel {

    public PropertiesPanel(String id, final NodeModel model, final ContentEditor contentEditor) {
        super(id, model);

        ArrayList<ITab> tabs = new ArrayList<ITab>();
        tabs.add(new AbstractTab(Model.of("Properties")) {
            @Override
            public WebMarkupContainer getPanel(String panelId) {
                return new InspectorPanel(panelId, model, contentEditor);
            }
        });
        tabs.add(new AbstractTab(Model.of("Versions")) {
            @Override
            public WebMarkupContainer getPanel(final String panelId) {
                return new WebMarkupContainer(panelId) {
                    @Override
                    public IMarkupFragment getMarkup() {
                        return Markup.of("<span wicket:id='" + panelId + "'>Versions....</span>");
                    }
                };
            }
        });
        add(new AjaxTabbedPanel<ITab>("properties-tabs", tabs));
    }
}
