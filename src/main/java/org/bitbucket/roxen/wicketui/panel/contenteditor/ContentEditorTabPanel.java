package org.bitbucket.roxen.wicketui.panel.contenteditor;

import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.nodetype.NodeType;
import javax.jcr.nodetype.PropertyDefinition;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.behavior.keyboardshortcut.KeyboardShortcutBehavior;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel;
import org.bitbucket.roxen.wicketui.panel.inspector.InspectorPanel;
import org.bitbucket.roxen.wicketui.panel.inspector.PropertiesPanel;

@SuppressWarnings("serial")
public class ContentEditorTabPanel extends Panel {

    private final static Logger LOG = Logger.getLogger(ContentEditorTabPanel.class.getName());

    private static final String TEMPLATE_PARAM = "wmod:template";

    private boolean isEditState = false;

    @Inject
    private Session session;

    private WebMarkupContainer viewStateButtons;
    private WebMarkupContainer editStateButtons;
    private Panel templatePanel;

    public ContentEditorTabPanel(String id, String path, final ContentEditor contentEditor) {
        super(id);
        setOutputMarkupId(true);

        final WebMarkupContainer editor = new WebMarkupContainer("editor");
        editor.setOutputMarkupId(true);

        templatePanel = createEditorPane("editor-pane", path, contentEditor);
        templatePanel.setOutputMarkupId(true);

        final Panel inspector = new PropertiesPanel("inspector", new NodeModel(path), contentEditor);
        inspector.setVisible(false);

        editor.add(templatePanel);
        editor.add(inspector);
        add(editor);

        Label js = new Label("js", Model.of("$(document).ready(function() { CM.initEditor('#" + editor.getMarkupId()
                + "');});"));
        js.setEscapeModelStrings(false);
        add(js);

        final AjaxSubmitLink propertiesButton = new AjaxSubmitLink("properties", contentEditor.getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                inspector.setVisible(!inspector.isVisible());
                templatePanel.setVisible(!templatePanel.isVisible());
                target.add(ContentEditorTabPanel.this);
            }
        };
        add(propertiesButton);

        viewStateButtons = new WebMarkupContainer("view-state-buttons");
        editStateButtons = new WebMarkupContainer("edit-state-buttons");

        final AjaxSubmitLink editButton = new AjaxSubmitLink("edit", contentEditor.getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                isEditState = true;
                target.add(contentEditor.getForm());
            }
        };
        editButton.setOutputMarkupId(true);
        editButton.add(new KeyboardShortcutBehavior("⌃+e/⌘+e"));
        viewStateButtons.add(editButton);

        final AjaxSubmitLink saveButton = new AjaxSubmitLink("save", contentEditor.getForm()) {
            @Inject
            private Session session;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    session.save();
                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Error saving session", e);
                }

                isEditState = false;
                target.add(contentEditor.getForm());
            }
        };
        saveButton.add(new KeyboardShortcutBehavior("⌃+s/⌘+s"));
        editStateButtons.add(saveButton);
        editStateButtons.add(new AjaxSubmitLink("cancel", contentEditor.getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                isEditState = false;
                target.add(contentEditor.getForm());
            }
        });

        add(viewStateButtons);
        add(editStateButtons);
    }

    @Override
    protected void onBeforeRender() {
        viewStateButtons.setVisible(!isEditState);
        templatePanel.setEnabled(isEditState);
        editStateButtons.setVisible(isEditState);

        super.onBeforeRender();
    }

    private Panel createEditorPane(String panelId, final String path, ContentEditor contentEditor) {
        try {
            Node node = session.getNode(path);
            String className = null;
            className = getTemplateNameFromNode(node);
            if (className != null) {
                Class<Panel> panelClass = (Class<Panel>) Class.forName(className);
                Constructor<Panel> constructor = panelClass.getConstructor(String.class, IModel.class);
                return constructor.newInstance(panelId, new NodeModel(path));
            } else {
                return new InspectorPanel(panelId, new NodeModel(node.getPath()), contentEditor);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error creating form panel", e);
        }
    }

    private String getTemplateNameFromNode(Node node) throws RepositoryException, ValueFormatException {
        NodeType primaryNodeType = node.getPrimaryNodeType();
        PropertyDefinition[] propertyDefinitions = primaryNodeType.getPropertyDefinitions();
        String className = null;
        for (PropertyDefinition propertyDefinition : propertyDefinitions) {
            if (propertyDefinition.getName().equals(TEMPLATE_PARAM)) {
                Value[] defaultValues = propertyDefinition.getDefaultValues();
                className = defaultValues[0].getString();
                break;
            }
        }
        return className;
    }

    public boolean isEditState() {
        return isEditState;
    }
}
