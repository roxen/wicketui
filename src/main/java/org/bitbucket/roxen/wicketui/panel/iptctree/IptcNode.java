package org.bitbucket.roxen.wicketui.panel.iptctree;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class IptcNode implements Serializable {

    private final String name;
    private final String id;
    private IptcNode parent;

    private final Map<String, IptcNode> nodes = new HashMap<String, IptcNode>();

    public IptcNode(final String name, final String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void addNode(IptcNode node) {
        getNodes().put(node.getId(), node);
    }

    public Map<String, IptcNode> getNodes() {
        return nodes;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setParent(IptcNode parent) {
        this.parent = parent;
    }

    public IptcNode getParent() {
        return parent;
    }
}
