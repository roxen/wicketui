package org.bitbucket.roxen.wicketui.panel.inbox;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;
import org.apache.wicket.protocol.ws.api.IWebSocketConnectionRegistry;
import org.apache.wicket.protocol.ws.api.SimpleWebSocketConnectionRegistry;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;

@SuppressWarnings("serial")
public class Inbox extends Panel {

    private final static Logger LOG = Logger.getLogger(Inbox.class.getName());

    @Inject
    private Session session;

    private final RepeatingView list;
    private final WebMarkupContainer listContainer;
    private final ContentEditor contentEditor;

    public Inbox(String id, ContentEditor contentEditor) {
        super(id);
        this.contentEditor = contentEditor;

        listContainer = new WebMarkupContainer("listcontainer");
        listContainer.setOutputMarkupId(true);
        add(listContainer);

        list = new RepeatingView("list");
        listContainer.add(list);

        add(new AjaxLink<String>("refresh") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                updateList(session);
                target.add(listContainer);
                target.appendJavaScript("$('abbr.timeago').timeago();");
            }
        });

        final WebSocketBehavior webSocketBehavior = new WebSocketBehavior() {

            @Override
            protected void onConnect(final ConnectedMessage message) {
                try {
                    session.getWorkspace().getObservationManager().addEventListener(new EventListener() {

                        @Override
                        public void onEvent(EventIterator events) {
                            IWebSocketConnectionRegistry registry = new SimpleWebSocketConnectionRegistry();
                            IWebSocketConnection wsConnection = registry.getConnection(message.getApplication(),
                                    message.getSessionId(), message.getPageId());
                            if (wsConnection != null && wsConnection.isOpen()) {
                                try {
                                    wsConnection.sendMessage("update");
                                } catch (IOException e) {
                                    LOG.log(Level.WARNING, "Error sending websocket message", e);
                                }
                            }
                        }
                    }, Event.NODE_ADDED | Event.PROPERTY_CHANGED | Event.NODE_REMOVED, null, true, null, null, false);
                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Error adding event listener", e);
                }
            }
        };
        add(webSocketBehavior);

        updateList(session);
    }

    private void updateList(Session session) {
        try {
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            String queryString = "SELECT * FROM [wmod:story] AS nodeSet ORDER BY nodeSet.[jcr:lastModified] DESC";
            Query query = queryManager.createQuery(queryString, Query.JCR_SQL2);
            query.setLimit(20);
            QueryResult result = query.execute();
            NodeIterator nodes = result.getNodes();
            list.removeAll();
            while (nodes.hasNext()) {
                Node node = nodes.nextNode();
                list.add(new InboxEntry(list.newChildId(), node, contentEditor));
            }
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error updating list", e);
        }
    }
}
