package org.bitbucket.roxen.wicketui.panel.fixedtabstabbedpanel;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

@SuppressWarnings("serial")
public class FixedTabsTabbedPanel<T extends ITab> extends AjaxTabbedPanel<T> {

    public FixedTabsTabbedPanel(String id, List<T> tabs) {
        super(id, tabs);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(FixedTabsTabbedPanel.class,
                "resources/fixed-tabs.js")));
        response.render(OnDomReadyHeaderItem.forScript("CM.initTabs('#" + getMarkupId() + "');"));
    }

    @Override
    protected void onAjaxUpdate(AjaxRequestTarget target) {
        target.appendJavaScript("CM.resizeTabs('#" + getMarkupId() + "');");
        super.onAjaxUpdate(target);
    }
}
