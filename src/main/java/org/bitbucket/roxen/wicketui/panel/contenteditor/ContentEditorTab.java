package org.bitbucket.roxen.wicketui.panel.contenteditor;

import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public abstract class ContentEditorTab extends AbstractTab {

    private final String tabId;
    private final boolean closable;

    protected Panel panel;

    public ContentEditorTab(IModel<String> title, String tabId) {
        this(title, tabId, true);
    }

    public ContentEditorTab(IModel<String> title, String tabId, boolean closable) {
        super(title);
        this.tabId = tabId;
        this.closable = closable;
    }

    public String getTabId() {
        return tabId;
    }

    public boolean isClosable() {
        return closable;
    }

    public Panel getPanel() {
        return panel;
    }
}
