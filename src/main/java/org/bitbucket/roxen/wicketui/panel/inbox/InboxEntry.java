package org.bitbucket.roxen.wicketui.panel.inbox;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;

import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.behavior.draggablenodebehavior.DraggableNodeBehavior;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;

@SuppressWarnings("serial")
public class InboxEntry extends Panel {

    private final static Logger logger = Logger.getLogger(InboxEntry.class.getName());

    public InboxEntry(String id, Node node, ContentEditor contentEditor) {
        super(id);

        try {
            String title = node.hasProperty("title") ? node.getProperty("title").getString() : "[" + node.getPath()
                    + "]";
            AjaxSubmitLink link = contentEditor.createOpenNodeLink("link", node.getPath());
            link.setBody(Model.of(title));
            add(link);
            add(new DraggableNodeBehavior());

            Date date = node.getProperty("jcr:lastModified").getDate().getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE hh:mm", Locale.getDefault());
            Label dateLabel = new Label("date", dateFormat.format(date));
            dateLabel.add(new AttributeAppender("title", DateFormat.getDateInstance().format(date)));
            add(dateLabel);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error rendering entry", e);
        }
    }
}
