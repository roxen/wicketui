package org.bitbucket.roxen.wicketui.panel.nodetree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.wicket.cdi.CdiContainer;
import org.apache.wicket.extensions.markup.html.repeater.tree.ITreeProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class NodeProvider implements ITreeProvider<String> {

    private final static Logger LOG = Logger.getLogger(NodeProvider.class.getName());

    private static final String CHILD_NODE_PATH = "section";

    private final String rootNodePath;

    @Inject
    private Session session;

    public NodeProvider(String rootNodePath) {
        CdiContainer.get().getNonContextualManager().inject(this);
        this.rootNodePath = rootNodePath;
    }

    @Override
    public void detach() {
    }

    @Override
    public Iterator<? extends String> getRoots() {
        return Arrays.asList(rootNodePath).iterator();
    }

    @Override
    public boolean hasChildren(String path) {
        try {
            Node node = session.getNode(path);
            if (node.getPrimaryNodeType().isNodeType("nt:folder")) {
                return node.hasNodes();
            }
            return node.hasNode(CHILD_NODE_PATH);
        } catch (ItemNotFoundException e) {
            LOG.log(Level.WARNING, "Error getting child nodes", e);
        } catch (RepositoryException e) {
            LOG.log(Level.WARNING, "Error getting child nodes", e);
        }

        return false;
    }

    @Override
    public Iterator<? extends String> getChildren(String path) {
        try {
            Node node = session.getNode(path);
            NodeIterator nodes;
            if (node.getPrimaryNodeType().isNodeType("nt:folder")) {
                nodes = node.getNodes();
            } else {
                nodes = node.getNodes(CHILD_NODE_PATH);
            }
            ArrayList<String> paths = new ArrayList<String>();
            while (nodes.hasNext()) {
                Node nextNode = nodes.nextNode();
                paths.add(nextNode.getPath());
            }
            return paths.iterator();
        } catch (ItemNotFoundException e) {
            LOG.log(Level.WARNING, "Error getting child nodes", e);
        } catch (RepositoryException e) {
            LOG.log(Level.WARNING, "Error getting child nodes", e);
        }

        return Collections.<String> emptyList().iterator();
    }

    @Override
    public IModel<String> model(String path) {
        return new Model<String>(path);
    }
}
