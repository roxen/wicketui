package org.bitbucket.roxen.wicketui.panel.inspector;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel;

import com.google.common.base.Joiner;

@SuppressWarnings("serial")
public class InspectorPanel extends Panel {

    private static Logger logger = Logger.getLogger(InspectorPanel.class.getName());
    private final ContentEditor contentEditor;
    private final NodeModel model;

    public InspectorPanel(String id, NodeModel model, ContentEditor contentEditor) {
        super(id, model);
        this.model = model;
        this.contentEditor = contentEditor;
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        Node node = model.getObject();

        RepeatingView propertyRows = new RepeatingView("properties");
        try {
            PropertyIterator properties = node.getProperties();
            while (properties.hasNext()) {
                Property property = properties.nextProperty();
                WebMarkupContainer row = new WebMarkupContainer(propertyRows.newChildId());
                row.add(new Label("name", property.getName()));

                String type = null;

                String value = "";
                if (property.isMultiple()) {
                    Value[] values = property.getValues();
                    Joiner joiner = Joiner.on(", ");
                    String valuesString = joiner.join(values);
                    value = valuesString;
                    if (values.length > 0) {
                        type = PropertyType.nameFromValue(values[0].getType());
                    }
                } else {
                    type = PropertyType.nameFromValue(property.getType());
                    switch (property.getType()) {
                    case PropertyType.BINARY:
                        value = "[binary]";
                        break;
                    default:
                        value = property.getString();
                    }
                }
                row.add(new Label("type", type));
                row.add(new Label("value", value));

                propertyRows.add(row);
            }
            addOrReplace(propertyRows);
        } catch (RepositoryException e) {
            logger.log(Level.WARNING, "Error listing properties.");
        }

        RepeatingView nodeRows = new RepeatingView("nodes");
        try {
            NodeIterator childNodes = node.getNodes();
            while (childNodes.hasNext()) {
                Node childNode = childNodes.nextNode();
                WebMarkupContainer row = new WebMarkupContainer(nodeRows.newChildId());
                row.add(new Label("name", childNode.getName()));
                row.add(new Label("path", childNode.getPath()));
                String value = "[node name]";
                if (childNode.hasProperty("title")) {
                    value = childNode.getProperty("title").getString();
                }
                AjaxSubmitLink nodeLink = contentEditor.createOpenNodeLink("link", childNode.getPath());
                nodeLink.setBody(Model.of(value));
                WebMarkupContainer linkContainer = new WebMarkupContainer("value");
                linkContainer.add(nodeLink);
                row.add(linkContainer);

                nodeRows.add(row);
            }
            addOrReplace(nodeRows);
        } catch (RepositoryException e) {
            logger.log(Level.WARNING, "Error listing properties.");
        }
    }
}
