package org.bitbucket.roxen.wicketui.panel.contenteditor;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.markup.html.tabs.TabbedPanel;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public class ContentEditorTabbedPanel<T> extends TabbedPanel<ContentEditorTab> {

    private final ContentEditor contentEditor;

    public ContentEditorTabbedPanel(final String id, ContentEditor contentEditor) {
        super(id, new ArrayList<ContentEditorTab>());
        this.contentEditor = contentEditor;

        setOutputMarkupId(true);
        setVersioned(false);
    }

    @Override
    protected WebMarkupContainer newLink(String linkId, final int index) {
        AjaxSubmitLink link = new AjaxSubmitLink(linkId, contentEditor.getForm()) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                setSelectedTab(index);
                target.add(ContentEditorTabbedPanel.this);
            }
        };

        return link;
    }

    @Override
    protected Component newTitle(String titleId, IModel<?> titleModel, int index) {
        return new ContentEditorTabTitlePanel(titleId, getIdOfTabWidhIndex(index), titleModel.getObject().toString(),
                contentEditor);
    }

    private ContentEditorTab getIdOfTabWidhIndex(int index) {
        return getTabs().get(index);
    }

    int getIndexOfTabWithId(String tabId) {
        List<ContentEditorTab> tabs = getTabs();
        int i = 0;
        for (ContentEditorTab tab : tabs) {
            if (tab.getTabId().equals(tabId)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}