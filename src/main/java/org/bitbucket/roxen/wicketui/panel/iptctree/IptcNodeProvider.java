package org.bitbucket.roxen.wicketui.panel.iptctree;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.wicket.extensions.markup.html.repeater.tree.ITreeProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

@SuppressWarnings("serial")
public class IptcNodeProvider implements ITreeProvider<IptcNode> {

    private static Map<String, IptcNode> roots = new HashMap<String, IptcNode>();
    private static Map<String, IptcNode> nodes = new HashMap<String, IptcNode>();
    private static List<IptcNode> nodeList = new ArrayList<IptcNode>();

    private final static Logger LOG = Logger.getLogger(IptcNodeProvider.class.getName());

    private static IptcNodeProvider instance;

    private IptcNodeProvider() {
        init();
    }

    public static IptcNodeProvider getInstance() {
        if (instance == null) {
            instance = new IptcNodeProvider();
        }

        return instance;
    }

    public List<IptcNode> getAllNodes() {
        return nodeList;
    }

    @Override
    public void detach() {
    }

    @Override
    public Iterator<? extends IptcNode> getRoots() {
        return roots.values().iterator();
    }

    @Override
    public boolean hasChildren(IptcNode node) {
        return !node.getNodes().isEmpty();
    }

    @Override
    public Iterator<? extends IptcNode> getChildren(IptcNode node) {
        return node.getNodes().values().iterator();
    }

    @Override
    public IModel<IptcNode> model(IptcNode object) {
        return new Model<IptcNode>(object);
    }

    private void init() {
        InputStream is = getClass().getResourceAsStream("iptc_codes.xml");
        try {
            Document document = Jsoup.parse(is, "UTF-8", "", Parser.xmlParser());
            Elements concepts = document.select("concept");

            ArrayList<Element> rest = new ArrayList<Element>();
            for (Element concept : concepts) {
                IptcNode node = getNode(concept);
                nodes.put(node.getId(), node);

                Element broaderElem = concept.select("broader").first();
                String broader = null;
                if (broaderElem != null) {
                    broader = broaderElem.attr("qcode");
                }
                if (broader != null) {
                    IptcNode parent = nodes.get(broader);
                    if (parent != null) {
                        parent.addNode(node);
                        node.setParent(parent);
                    } else {
                        rest.add(concept);
                    }
                } else {
                    roots.put(node.getId(), node);
                }
            }

            for (Element concept : rest) {
                String broader = concept.select("broader").first().attr("qcode");
                IptcNode parent = nodes.get(broader);
                if (parent != null) {
                    IptcNode node = getNode(concept);
                    parent.addNode(node);
                    node.setParent(parent);
                } else {
                    LOG.log(Level.WARNING, "Warning, broader concept not found: " + broader);
                }
            }

            createList(roots);
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Error initializing IPTC tree", e);
        }
    }

    private void createList(Map<String, IptcNode> nodes) {
        Collection<IptcNode> values = nodes.values();
        for (IptcNode node : values) {
            nodeList.add(node);
            createList(node.getNodes());
        }
    }

    private IptcNode getNode(Element concept) {
        String id = concept.select("conceptId").first().attr("qcode");
        String name = concept.select("name[xml:lang=en-GB]").text();
        IptcNode node = new IptcNode(name, id);
        return node;
    }
}
