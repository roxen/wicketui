package org.bitbucket.roxen.wicketui.panel.contenteditor.model;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.wicket.Component;
import org.apache.wicket.cdi.CdiContainer;
import org.apache.wicket.model.IComponentInheritedModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.IWrapModel;

@SuppressWarnings("serial")
public class NodeModel implements IModel<Node>, IComponentInheritedModel<Node> {

    private final static Logger LOG = Logger.getLogger(NodeModel.class.getName());

    private final String path;

    @Inject
    private Session session;

    public NodeModel(String path) {
        CdiContainer.get().getNonContextualManager().inject(this);
        this.path = path;
    }

    public Node getObject() {
        try {
            return session.getNode(path);
        } catch (ItemNotFoundException e) {
            LOG.log(Level.WARNING, "Error getting node model", e);
        } catch (RepositoryException e) {
            LOG.log(Level.WARNING, "Error getting node model", e);
        }

        return null;
    }

    public void setObject(Node object) {
    }

    public void detach() {
    }

    @SuppressWarnings("unchecked")
    public IWrapModel<String> wrapOnInheritance(Component component) {
        return new WrappingStringPropertyModel(component);
    }

    public class WrappingStringPropertyModel implements IWrapModel<String> {

        private final Component owner;

        public WrappingStringPropertyModel(Component owner) {
            this.owner = owner;
        }

        public IModel<Node> getWrappedModel() {
            return NodeModel.this;
        }

        public String getObject() {

            try {
                Node node = NodeModel.this.getObject();
                if (node.hasProperty(getOwnerId())) {
                    Property property = node.getProperty(getOwnerId());
                    if (property.isMultiple()) {
                        LOG.log(Level.WARNING, "Multivalued properties not supported yet");
                        return null;
                    }
                    return property.getString();
                }
            } catch (PathNotFoundException e) {
                LOG.log(Level.WARNING, "Error getting property", e);
            } catch (RepositoryException e) {
                LOG.log(Level.WARNING, "Error getting property", e);
            }

            return null;
        }

        private String getOwnerId() {
            return owner.getId();
        }

        public void setObject(String object) {
            try {
                Node node = NodeModel.this.getObject();
                node.setProperty(getOwnerId(), object == null ? null : object, PropertyType.STRING);
            } catch (Exception e) {
                LOG.log(Level.WARNING, "Error setting property", e);
            }
        }

        public void detach() {
            NodeModel.this.detach();
        }
    }
}
