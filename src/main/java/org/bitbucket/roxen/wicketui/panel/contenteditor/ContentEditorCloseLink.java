package org.bitbucket.roxen.wicketui.panel.contenteditor;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.form.Form;

@SuppressWarnings("serial")
public final class ContentEditorCloseLink extends AjaxSubmitLink {

    private final ContentEditor contentEditor;
    private String tabId;

    public ContentEditorCloseLink(String id, String tabId, ContentEditor contentEditor) {
        super(id, contentEditor.getForm());
        this.tabId = tabId;
        this.contentEditor = contentEditor;
    }

    @Override
    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
        contentEditor.closeTab(tabId, target);
        if (target != null) {
            target.add(contentEditor.getForm());
        }
    }
}
