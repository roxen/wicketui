(function(CM) {
    CM.initEditor = function(editorId) {
        var resizeEditor = function(editor) {
            var toolbar = editor.siblings('.toolbar');
            var tabRow = editor.parent().siblings('.tab-row');
            var height = editor.closest('.ui-layout-pane').height()
                    - toolbar.outerHeight() - tabRow.outerHeight();
            editor.css('position', 'absolute').css('overflow', 'auto');
            editor.outerHeight(height);
        };
        var editor = $(editorId);
        var paneElem = editor.closest('.ui-layout-pane');
        if (paneElem.size() > 0) {
            var pane = paneElem.data('layoutPane');
            pane.options.onresize = function() {
                resizeEditor(editor);
            };
            resizeEditor(editor);
        }
    };
}(window.CM = window.CM || {}));
