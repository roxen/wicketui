package org.bitbucket.roxen.wicketui.panel.contenteditor.model;

import java.util.Map;

import org.apache.wicket.Component;
import org.apache.wicket.model.IComponentInheritedModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.IWrapModel;

@SuppressWarnings("serial")
public class MapComponentModel implements IModel, IComponentInheritedModel {

    private final Map<String, String> map;

    public MapComponentModel(Map<String, String> map) {
        this.map = map;
    }

    public Object getObject() {
        // Lazy load
        return map;
    }

    public void setObject(Object object) {
    }

    public void detach() {
    }

    public IWrapModel wrapOnInheritance(Component component) {
        return new AttachedJcrPropertyModel(component);
    }

    private class AttachedJcrPropertyModel implements IWrapModel {

        private final Component owner;

        public AttachedJcrPropertyModel(Component owner) {
            this.owner = owner;
        }

        public IModel getWrappedModel() {
            return MapComponentModel.this;
        }

        public Object getObject() {

            return map.get(owner.getId());
        }

        public void setObject(Object object) {
            try {
                map.put(owner.getId(), object == null ? null : object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void detach() {
            MapComponentModel.this.detach();
        }
    }
}
