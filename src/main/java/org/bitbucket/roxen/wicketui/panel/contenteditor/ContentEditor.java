package org.bitbucket.roxen.wicketui.panel.contenteditor;

import java.util.List;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.bitbucket.roxen.wicketui.behavior.keyboardshortcut.KeyboardShortcutBehavior;

@SuppressWarnings("serial")
public class ContentEditor extends Panel {

    @Inject
    private Session session;

    private final ContentEditorTabbedPanel<ITab> tabbedPanel;
    private final Panel placeholderPanel;
    private final Form<Void> form;

    public ContentEditor(String id) {
        super(id);
        setOutputMarkupId(true);

        tabbedPanel = new ContentEditorTabbedPanel<ITab>("tab-panel", this);

        form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                try {
                    session.save();
                } catch (Exception e) {
                    throw new RuntimeException("Error saving session", e);
                }
            };
        };

        placeholderPanel = new PlaceholderPanel("placeholder-panel");
        placeholderPanel.setVisible(false);
        placeholderPanel.setOutputMarkupId(true);
        add(placeholderPanel);

        form.add(tabbedPanel);
        form.add(placeholderPanel);
        add(form);

        add(new KeyboardShortcutBehavior("⌃+u/⌘+u", "$('#" + form.getMarkupId() + " li.selected .close').click()"));
        add(new KeyboardShortcutBehavior("⌃+i/⌘+i", "$('#" + form.getMarkupId() + " .properties').click()"));

        AjaxSubmitLink leftLink = new AjaxSubmitLink("left", form) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                int selectedTab = tabbedPanel.getSelectedTab();
                if (selectedTab != 0 && tabbedPanel.getTabs().size() > 1) {
                    tabbedPanel.setSelectedTab(selectedTab - 1);
                    target.add(form);
                }
            }
        };
        leftLink.setOutputMarkupId(true);
        add(leftLink);
        add(new KeyboardShortcutBehavior("⌃+←/⌘+←", "$('#" + leftLink.getMarkupId() + "').click()"));
        AjaxSubmitLink rightLink = new AjaxSubmitLink("right", form) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                int selectedTab = tabbedPanel.getSelectedTab();
                if (selectedTab != tabbedPanel.getTabs().size() - 1 && tabbedPanel.getTabs().size() > 1) {
                    tabbedPanel.setSelectedTab(selectedTab + 1);
                    target.add(form);
                }
            }
        };
        rightLink.setOutputMarkupId(true);
        add(rightLink);
        add(new KeyboardShortcutBehavior("⌃+→/⌘+→", "$('#" + rightLink.getMarkupId() + "').click()"));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(ContentEditor.class,
                "contenteditor.js")));
    }

    public Form<Void> getForm() {
        return form;
    }

    public void closeTab(String tabId, AjaxRequestTarget target) {
        List<ContentEditorTab> tabs = tabbedPanel.getTabs();
        int tabIndex = tabbedPanel.getIndexOfTabWithId(tabId);
        if (tabIndex != -1) {
            tabs.remove(tabIndex);
            int tabCount = tabs.size();
            if (tabCount > 0) {
                int selectedTab = tabbedPanel.getSelectedTab();
                selectedTab = Math.min(selectedTab, tabCount - 1);
                tabbedPanel.setSelectedTab(selectedTab);
            }

            if (tabbedPanel.getTabs().size() == 0) {
                tabbedPanel.setVisible(false);
                placeholderPanel.setVisible(true);
            }

            if (target != null) {
                target.add(form);
            }
        }
    }

    public void openTab(final ContentEditorTab tab, AjaxRequestTarget target) {
        List<ContentEditorTab> tabs = tabbedPanel.getTabs();
        int tabIndex = tabbedPanel.getIndexOfTabWithId(tab.getTabId());
        if (tabIndex != -1) {
            tabbedPanel.setSelectedTab(tabIndex);
            if (target != null) {
                target.add(form);
            }
            return;
        }

        tabs.add(tab);
        tabbedPanel.setSelectedTab(tabs.size() - 1);

        tabbedPanel.setVisible(true);
        placeholderPanel.setVisible(false);

        if (target != null) {
            target.add(form);
            target.add(form);
        }
    }

    public void setSelectedTab(String tabId, AjaxRequestTarget target) {
        int tabIndex = tabbedPanel.getIndexOfTabWithId(tabId);
        if (tabIndex != -1) {
            tabbedPanel.setSelectedTab(tabIndex);
            if (target != null) {
                target.add(form);
            }
        }
    }

    public AjaxSubmitLink createOpenNodeLink(String id, final String path) {
        return new AjaxSubmitLink(id, getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                openNode(path, target);
            }
        };
    }

    public void openNode(final String path, AjaxRequestTarget target) {
        String nodeName = getNodeName(path);
        ContentEditorTab tab = new ContentEditorTab(Model.of(nodeName), path) {

            @Override
            public WebMarkupContainer getPanel(String panelId) {
                if (panel == null) {
                    panel = new ContentEditorTabPanel(panelId, path, ContentEditor.this);
                }
                return panel;
            }
        };
        openTab(tab, target);
    }

    private String getNodeName(final String path) {
        String nodeName = path;
        try {
            Node node = session.getNode(path);
            if (node.hasProperty("title")) {
                nodeName = node.getProperty("title").getString();
            }
        } catch (Exception e) {
            throw new RuntimeException("Error creating form panel", e);
        }
        return nodeName;
    }
}
