package org.bitbucket.roxen.wicketui.connector;

import java.util.Collection;
import java.util.Set;

import org.infinispan.schematic.document.Document;
import org.modeshape.jcr.federation.spi.DocumentChanges;
import org.modeshape.jcr.federation.spi.DocumentChanges.PropertyChanges;
import org.modeshape.jcr.federation.spi.DocumentWriter;
import org.modeshape.jcr.federation.spi.WritableConnector;
import org.modeshape.jcr.value.Name;

public class MapConnector extends WritableConnector {

    @Override
    public Document getDocumentById(String id) {
        if (!"themap".equals(id)) {
            return null;
        }

        DocumentWriter documentWriter = newDocument(id);
        documentWriter.addProperty("title", "This is the map doc");
        documentWriter.addProperty("description", "This is the map connector root node description.");
        documentWriter.setPrimaryType("wmod:section");

        return documentWriter.document();
    }

    @Override
    public String getDocumentId(String path) {
        System.out.println("getDocumentId: " + path);
        return "themap";
    }

    @Override
    public boolean removeDocument(String id) {
        System.out.println("removeDocument: " + id);
        return false;
    }

    @Override
    public boolean hasDocument(String id) {
        System.out.println("hasDocument: " + id);
        if (id.equals("themap")) {
            return true;
        }
        return false;
    }

    @Override
    public void storeDocument(Document document) {
        System.out.println("storeDocument: " + document);
    }

    @Override
    public void updateDocument(DocumentChanges documentChanges) {
        PropertyChanges propertyChanges = documentChanges.getPropertyChanges();
        Set<Name> changedProperties = propertyChanges.getChanged();
        System.out.println(changedProperties.size());
        for (Name name : changedProperties) {
            String localName = name.getLocalName();
            System.out.println(localName + ": " + name.getString());
        }
        System.out.println("updateDocument: " + documentChanges.getPropertyChanges());
    }

    @Override
    public String newDocumentId(String parentId, Name newDocumentName, Name newDocumentPrimaryType) {
        System.out.println("newDocumentId: " + parentId);
        return null;
    }

    @Override
    public Collection<String> getDocumentPathsById(String id) {
        System.out.println("getDocumentPathsById: " + id);
        return null;
    }
}
