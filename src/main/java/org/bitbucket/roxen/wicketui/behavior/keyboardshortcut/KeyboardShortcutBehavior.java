package org.bitbucket.roxen.wicketui.behavior.keyboardshortcut;

import static java.util.Arrays.asList;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.WicketEventJQueryResourceReference;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

@SuppressWarnings("serial")
public class KeyboardShortcutBehavior extends Behavior {

    private static final KeyboardShortcutResourceReference KEYBOARD_SHORTCUT_RESOURCE_REFERENCE = new KeyboardShortcutResourceReference();

    private final String shortcut;
    private final String action;

    public KeyboardShortcutBehavior(String shortcut) {
        this.shortcut = shortcut;
        this.action = null;
    }

    public KeyboardShortcutBehavior(String shortcut, String action) {
        this.shortcut = shortcut;
        this.action = action;
    }

    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        super.renderHead(component, response);
        response.render(JavaScriptHeaderItem.forReference(KEYBOARD_SHORTCUT_RESOURCE_REFERENCE));
        String action = this.action;
        if (action == null) {
            action = "$('#" + component.getMarkupId() + "').click()";
        }
        response.render(JavaScriptHeaderItem.forScript("jwerty.key('" + shortcut + "', function (e) { " + action
                + "; e.preventDefault(); });", null));
    }

    private static class KeyboardShortcutResourceReference extends JavaScriptResourceReference {

        public KeyboardShortcutResourceReference() {
            super(KeyboardShortcutBehavior.class, "jwerty.js");
        }

        @Override
        public Iterable<? extends HeaderItem> getDependencies() {
            HeaderItem jquery = JavaScriptHeaderItem.forReference(WicketEventJQueryResourceReference.get());
            return asList(jquery);
        }
    }

}
