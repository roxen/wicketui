package org.bitbucket.roxen.wicketui.behavior.select2bootstrap;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.request.resource.CssResourceReference;

@SuppressWarnings("serial")
public class Select2BootstrapBehavior extends Behavior {
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(CssHeaderItem.forReference(new CssResourceReference(Select2BootstrapBehavior.class,
                "select2-bootstrap.css")));
    }
}
