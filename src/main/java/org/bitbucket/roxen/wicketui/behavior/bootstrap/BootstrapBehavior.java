package org.bitbucket.roxen.wicketui.behavior.bootstrap;

import static java.util.Arrays.asList;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.WicketEventJQueryResourceReference;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

@SuppressWarnings("serial")
public class BootstrapBehavior extends Behavior {

    private static final CssResourceReference BOOTSTRAP_CSS_RESOURCE_REFERENCE = new CssResourceReference(
            BootstrapBehavior.class, "css/bootstrap.css");
    private static final BootstrapResourceReference BOOTSTRAP_RESOURCE_REFERENCE = new BootstrapResourceReference();

    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forReference(BOOTSTRAP_RESOURCE_REFERENCE));
    }

    private static class BootstrapResourceReference extends JavaScriptResourceReference {

        public BootstrapResourceReference() {
            super(BootstrapBehavior.class, "js/bootstrap.js");
        }

        @Override
        public Iterable<? extends HeaderItem> getDependencies() {
            HeaderItem jquery = JavaScriptHeaderItem.forReference(WicketEventJQueryResourceReference.get());
            HeaderItem stylesheet = CssHeaderItem.forReference(BOOTSTRAP_CSS_RESOURCE_REFERENCE);

            return asList(jquery, stylesheet);
        }
    }
}
