package org.bitbucket.roxen.wicketui.behavior.draggablenodebehavior;

import org.apache.wicket.Component;
import org.odlabs.wiquery.ui.draggable.DraggableBehavior;
import org.odlabs.wiquery.ui.draggable.DraggableHelper;

@SuppressWarnings("serial")
public class DraggableNodeBehavior extends DraggableBehavior {
    @Override
    public void onConfigure(Component component) {
        super.onConfigure(component);
        setHelper(DraggableHelper.CLONE);
        setZIndex(2);
        setConnectToSortable(".connectedSortable");
        setAppendTo("body");
    }
}
