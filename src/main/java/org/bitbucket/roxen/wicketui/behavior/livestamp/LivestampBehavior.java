package org.bitbucket.roxen.wicketui.behavior.livestamp;

import static java.util.Arrays.asList;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.WicketEventJQueryResourceReference;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

@SuppressWarnings("serial")
public class LivestampBehavior extends Behavior {

    private static final LivestampReference LIVESTAMP_RESOURCE_REFERENCE = new LivestampReference();
    private static final JavaScriptResourceReference MOMENT_JS_RESOURCE_REFERENCE = new JavaScriptResourceReference(
            LivestampBehavior.class, "moment.min.js");

    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forReference(MOMENT_JS_RESOURCE_REFERENCE));
        response.render(JavaScriptHeaderItem.forReference(LIVESTAMP_RESOURCE_REFERENCE));
    }

    private static class LivestampReference extends JavaScriptResourceReference {

        public LivestampReference() {
            super(LivestampBehavior.class, "livestamp.min.js");
        }

        @Override
        public Iterable<? extends HeaderItem> getDependencies() {
            return asList(JavaScriptHeaderItem.forReference(WicketEventJQueryResourceReference.get()));
        }
    }
}
