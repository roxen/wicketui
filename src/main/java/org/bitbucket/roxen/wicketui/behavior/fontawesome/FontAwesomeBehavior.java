package org.bitbucket.roxen.wicketui.behavior.fontawesome;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.request.resource.CssResourceReference;

@SuppressWarnings("serial")
public class FontAwesomeBehavior extends Behavior {
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        response.render(CssHeaderItem.forReference(new CssResourceReference(FontAwesomeBehavior.class,
                "css/font-awesome.css")));
    }
}
