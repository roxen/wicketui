package org.bitbucket.roxen.wicketui.defaultui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.jcr.version.Version;
import javax.jcr.version.VersionHistory;
import javax.jcr.version.VersionIterator;
import javax.jcr.version.VersionManager;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.extensions.markup.html.tabs.TabbedPanel;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditorTab;
import org.bitbucket.roxen.wicketui.panel.fixedtabstabbedpanel.FixedTabsTabbedPanel;
import org.bitbucket.roxen.wicketui.panel.label.LabelPanel;
import org.bitbucket.roxen.wicketui.panel.nodetree.NodeTreePanel;
import org.bitbucket.roxen.wicketui.util.FileUtil;
import org.modeshape.jcr.api.Problem;
import org.modeshape.jcr.api.Problems;
import org.modeshape.jcr.api.RepositoryManager;

@SuppressWarnings("serial")
public class NavPanel extends Panel {

    private static final Logger LOG = Logger.getLogger(NavPanel.class.getName());

    @Inject
    private Session session;

    public NavPanel(String id, final String path, final ContentEditor contentEditor) {
        super(id);

        final TabbedPanel<ITab> tabbedPanel = new FixedTabsTabbedPanel<ITab>("tabs", new ArrayList<ITab>());
        tabbedPanel.getTabs().add(new AbstractTab(Model.of("Content")) {
            Panel panel;

            @Override
            public WebMarkupContainer getPanel(String panelId) {
                if (panel == null) {
                    panel = new NodeTreePanel(panelId, "/sites/mst", contentEditor);
                }
                return panel;
            }
        });
        tabbedPanel.getTabs().add(new AbstractTab(Model.of("Map content")) {
            Panel panel;

            @Override
            public WebMarkupContainer getPanel(String panelId) {
                if (panel == null) {
                    panel = new NodeTreePanel(panelId, "/map", contentEditor);
                }
                return panel;
            }
        });
        tabbedPanel.getTabs().add(new AbstractTab(Model.of("Files")) {
            Panel panel;

            @Override
            public WebMarkupContainer getPanel(String panelId) {
                if (panel == null) {
                    panel = new NodeTreePanel(panelId, "/working-directory", contentEditor);
                }
                return panel;
            }
        });
        add(tabbedPanel);

        add(new AjaxSubmitLink("open-pane", contentEditor.getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                final String tabId = UUID.randomUUID().toString();
                contentEditor.openTab(new ContentEditorTab(Model.of("New pane " + tabId.substring(0, 4)), tabId) {

                    private LabelPanel panel;

                    @Override
                    public WebMarkupContainer getPanel(String panelId) {
                        if (panel == null) {
                            panel = new LabelPanel(panelId, "tab with id: " + tabId);
                        }
                        return panel;
                    }
                }, target);
            }
        });

        add(new AjaxLink<String>("backup") {
            @Override
            public void onClick(AjaxRequestTarget target) {

                try {
                    RepositoryManager repoManager = ((org.modeshape.jcr.api.Workspace) session.getWorkspace())
                            .getRepositoryManager();
                    File tempDirectory = FileUtil.createTempDirectory();
                    Problems problems = repoManager.backupRepository(tempDirectory);
                    if (problems.hasProblems()) {
                        System.out.println("Problems backing up repository:");
                        for (Problem problem : problems) {
                            System.out.println(problem.getMessage());
                        }
                    } else {
                        System.out.println("Backed up repository to " + tempDirectory.getPath());
                        File zipFile = File.createTempFile("backup-" + System.currentTimeMillis(), ".zip");
                        System.out.println("Zipped backup to " + zipFile.getPath());
                        FileUtil.zipFolder(tempDirectory.getPath(), zipFile.getPath());
                    }
                } catch (AccessDeniedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (RepositoryException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        add(new AjaxLink<String>("connect") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                try {
                    Node node = session.getNode("/map");
                    System.out.println(node.getName());
                } catch (RepositoryException e) {
                    LOG.log(Level.WARNING, "Error testing connector", e);
                }
            }
        });

        add(new AjaxLink<String>("create-content") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                try {
                    Node siteNode = session.getNode(DefaultApplicationPage.ROOT_NODE_PATH);
                    assert siteNode != null;

                    byte[] imageBytes = createImageByteArray();
                    long start = System.currentTimeMillis();
                    Node lastNode = null;
                    for (int i = 0; i < 1; i++) {
                        Node node = siteNode.addNode("content", "wmod:story");
                        node.setProperty("title", "New node at " + new Date().toString());
                        node.setProperty("image",
                                session.getValueFactory().createBinary(new ByteArrayInputStream(imageBytes)));
                        if (lastNode == null) {
                            lastNode = node;
                        }
                    }
                    session.save();

                    long stop = System.currentTimeMillis();
                    System.out.println((stop - start) + "ms");

                    if (lastNode != null) {
                        contentEditor.openNode(lastNode.getPath(), target);
                    }
                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Error creating content", e);
                }
            }

            private byte[] createImageByteArray() throws IOException {
                BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_ARGB);
                Graphics graphics = image.getGraphics();
                graphics.setColor(Color.GREEN);
                graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
                graphics.setColor(Color.BLACK);
                graphics.drawLine(0, 0, image.getWidth(), image.getHeight());
                graphics.drawLine(0, image.getHeight(), image.getWidth(), 0);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(image, "png", os);
                byte[] imageBytes = os.toByteArray();
                return imageBytes;
            }
        });

        add(new AjaxLink<String>("test-ordering") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                try {
                    Node node1 = session.getRootNode().addNode("def", "wmod:story");
                    Node node2 = session.getRootNode().addNode("def", "wmod:story");
                    Node node3 = session.getRootNode().addNode("def", "wmod:story");

                    session.save();

                    ValueFactory valueFactory = session.getValueFactory();
                    Value node2ref = valueFactory.createValue(node2);
                    Value node3ref = valueFactory.createValue(node3);

                    node1.setProperty("refs", new Value[] { node3ref, node2ref });
                    session.save();

                    System.out.println("--------before");
                    Value[] values = node1.getProperty("refs").getValues();
                    for (Value value : values) {
                        System.out.println(value);
                    }

                    Collections.reverse(Arrays.asList(values));

                    System.out.println("--------reversed");
                    for (Value value : values) {
                        System.out.println(value);
                    }

                    node1.setProperty("refs", new Value[] {});
                    session.save();

                    node1.setProperty("refs", new Value[] { node2ref, node3ref });
                    session.save();
                    System.out.println("--------after");
                    values = node1.getProperty("refs").getValues();
                    for (Value value : values) {
                        System.out.println(value);
                    }

                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Error testing node ordering", e);
                }
            }
        });

        add(new AjaxSubmitLink("test-versioned", contentEditor.getForm()) {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

                try {
                    Node node = session.getRootNode().addNode("vstuff", "wmod:vstory");
                    VersionManager versionManager = session.getWorkspace().getVersionManager();
                    node.setProperty("title", "This is the title");
                    System.out.println("is checked out: " + node.isCheckedOut());
                    session.save();

                    versionManager.checkout(node.getPath());

                    node.setProperty("title", "This is the title. Updated.");
                    System.out.println("is checked out: " + node.isCheckedOut());
                    session.save();

                    versionManager.checkpoint(node.getPath());

                    node.setProperty("title", "This is the title. Updated again.");
                    System.out.println("is checked out: " + node.isCheckedOut());
                    session.save();

                    versionManager.checkin(node.getPath());
                    System.out.println("is checked out: " + node.isCheckedOut());

                    VersionHistory versionHistory = versionManager.getVersionHistory(node.getPath());
                    VersionIterator linearVersions = versionHistory.getAllLinearVersions();
                    while (linearVersions.hasNext()) {
                        Version nextVersion = linearVersions.nextVersion();
                        System.out.println("---- " + nextVersion);
                    }

                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Error testing versioned content", e);
                }
            }
        });
    }
}
