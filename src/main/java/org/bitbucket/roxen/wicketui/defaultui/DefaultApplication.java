package org.bitbucket.roxen.wicketui.defaultui;

import org.apache.wicket.markup.html.WebPage;
import org.bitbucket.roxen.wicketui.application.CmApplication;

public class DefaultApplication extends CmApplication {
    @Override
    public Class<? extends WebPage> getHomePage() {
        return DefaultApplicationPage.class;
    }
}
