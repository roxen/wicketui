package org.bitbucket.roxen.wicketui.defaultui;

import java.util.Date;
import java.util.UUID;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.bitbucket.roxen.wicketui.behavior.bootstrap.BootstrapBehavior;
import org.bitbucket.roxen.wicketui.behavior.fontawesome.FontAwesomeBehavior;
import org.bitbucket.roxen.wicketui.behavior.livestamp.LivestampBehavior;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditor;
import org.bitbucket.roxen.wicketui.panel.contenteditor.ContentEditorTab;
import org.bitbucket.roxen.wicketui.panel.inbox.Inbox;
import org.bitbucket.roxen.wicketui.panel.label.LabelPanel;
import org.bitbucket.roxen.wicketui.panel.layout.Layout;
import org.bitbucket.roxen.wicketui.panel.layout.Layout.PanePositionEnum;

import de.agilecoders.wicket.less.LessResourceReference;

@SuppressWarnings("serial")
public class DefaultApplicationPage extends WebPage {

    public static final String ROOT_NODE_PATH = "/sites/mst";

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forReference(new LessResourceReference(DefaultApplicationPage.class,
                "resources/cm-application.less")));
    }

    public DefaultApplicationPage() {
        add(new FontAwesomeBehavior());
        add(new BootstrapBehavior());
        add(new LivestampBehavior());
        add(new Layout("layout", true) {

            private ContentEditor contentEditor;

            @Override
            public WebMarkupContainer getLayoutCenterComponent(String wicketId) {
                return new Layout(wicketId, false) {
                    @Override
                    public WebMarkupContainer getLayoutCenterComponent(String wicketId) {
                        contentEditor = new ContentEditor(wicketId);
                        addExampleTabs(contentEditor);
                        return contentEditor;
                    }

                    @Override
                    public WebMarkupContainer getLayoutSouthComponent(String wicketId) {
                        LabelPanel southPanel = new LabelPanel(wicketId, "Really useful stuff here");
                        southPanel.getLabel().add(new AttributeAppender("class", Model.of("text-muted")));
                        return southPanel;
                    }

                    @Override
                    public WebMarkupContainer getLayoutEastComponent(String wicketId) {
                        return new NavPanel(wicketId, ROOT_NODE_PATH, contentEditor);
                    };
                }.setSize(PanePositionEnum.SOUTH, 200).setSize(PanePositionEnum.EAST, 280).setSpacingOpen(1)
                        .setInitClosed(PanePositionEnum.EAST, true).setInitClosed(PanePositionEnum.SOUTH, true);
            }

            @Override
            public WebMarkupContainer getLayoutWestComponent(String wicketId) {
                return new Layout(wicketId, false) {

                    @Override
                    public WebMarkupContainer getLayoutCenterComponent(String wicketId) {
                        return new NavPanel(wicketId, ROOT_NODE_PATH, contentEditor);
                    }

                    @Override
                    public WebMarkupContainer getLayoutSouthComponent(String wicketId) {
                        return new Inbox(wicketId, contentEditor);
                    }
                }.setSize(PanePositionEnum.SOUTH, 280).setSpacingOpen(1);
            }

            @Override
            public WebMarkupContainer getLayoutNorthComponent(String wicketId) {
                return new LabelPanel(wicketId, "Content Manager");
            }
        }.setSize(PanePositionEnum.WEST, 320).setSize(PanePositionEnum.NORTH, 40)
                .setClosable(PanePositionEnum.NORTH, false).setResizable(PanePositionEnum.NORTH, false)
                .setSpacingOpen(1).setSpacingOpen(PanePositionEnum.NORTH, 0));
    }

    private void addExampleTabs(ContentEditor contentEditor) {
        String firstTabId = null;
        for (int i = 0; i < 3; i++) {
            String tabId = UUID.randomUUID().toString();
            contentEditor.openTab(new ContentEditorTab(Model.of("Example tab " + i), tabId) {
                private LabelPanel labelPanel;

                @Override
                public WebMarkupContainer getPanel(String panelId) {
                    if (labelPanel == null) {
                        labelPanel = new LabelPanel(panelId, "Editor tab created at " + new Date().toString());
                    }
                    return labelPanel;
                }
            }, null);
            if (firstTabId == null) {
                firstTabId = tabId;
            }
        }
        if (firstTabId != null) {
            contentEditor.setSelectedTab(firstTabId, null);
        }
    }
}
