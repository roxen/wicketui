package org.bitbucket.roxen.wicketui.cdi;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.modeshape.web.jcr.NoSuchRepositoryException;
import org.modeshape.web.jcr.RepositoryManager;

@Singleton
public class SessionProducer {

    private Repository repository;

    @PostConstruct
    public void initializeRepository() throws NoSuchRepositoryException {
        repository = RepositoryManager.getRepository(null);
    }

    @RequestScoped
    @Produces
    public Session getSession() throws RepositoryException {
        return repository.login();
    }

    public void logoutSession(@Disposes final Session session) {
        session.logout();
    }
}