package org.bitbucket.roxen.wicketui.form.nodelist;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class NodeReferenceListEntryPanel extends Panel {

    private final static Logger logger = Logger.getLogger(NodeReferenceListEntryPanel.class.getName());

    @Inject
    private Session session;

    private AjaxSubmitLink removeLink;

    public NodeReferenceListEntryPanel(String id, IModel<NodeReferenceListEntryModel> model) {
        super(id, model);

        try {
            Node node = session.getNodeByIdentifier(model.getObject().getIdentifier());
            Label titleLabel = new Label("title", node.getProperty("title").getString());
            titleLabel.add(new AttributeAppender("title", Model.of(node.getIdentifier())));
            add(titleLabel);
            add(new Label("id", node.getIdentifier()));

            removeLink = new AjaxSubmitLink("remove") {
                @Override
                protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                    super.onSubmit(target, form);
                    System.out.println("clicked delete....");
                }
            };
            removeLink.setVisible(isEnabledInHierarchy());
            add(removeLink);

        } catch (ValueFormatException e) {
            logger.log(Level.WARNING, "Error creating entry", e);
        } catch (PathNotFoundException e) {
            logger.log(Level.WARNING, "Error creating entry", e);
        } catch (RepositoryException e) {
            logger.log(Level.WARNING, "Error creating entry", e);
        }
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        removeLink.setVisible(isEnabledInHierarchy());
    }
}
