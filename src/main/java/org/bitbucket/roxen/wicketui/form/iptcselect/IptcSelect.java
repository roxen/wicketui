package org.bitbucket.roxen.wicketui.form.iptcselect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.model.IModel;
import org.bitbucket.roxen.wicketui.behavior.select2bootstrap.Select2BootstrapBehavior;
import org.bitbucket.roxen.wicketui.panel.iptctree.IptcNode;
import org.bitbucket.roxen.wicketui.panel.iptctree.IptcNodeProvider;

import com.vaynberg.wicket.select2.Response;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import com.vaynberg.wicket.select2.TextChoiceProvider;

@SuppressWarnings({ "unchecked", "serial" })
public class IptcSelect extends FormComponentPanel<String> {
    private static final String MULTI_VALUE_DELIMITER = ",";

    public IptcSelect(String id) {
        super(id);
        add(new Select2BootstrapBehavior());

        @SuppressWarnings("rawtypes")
        Select2MultiChoice<IptcNode> multiChoice = new Select2MultiChoice<IptcNode>("categories") {
            @Override
            protected void onInitialize() {
                super.onInitialize();

                final IModel origModel = getModel();
                setModel(new IModel<Collection<IptcNode>>() {

                    @Override
                    public void detach() {
                    }

                    @Override
                    public void setObject(Collection<IptcNode> object) {
                        StringBuilder sb = new StringBuilder();
                        for (IptcNode node : object) {
                            if (sb.length() > 0) {
                                sb.append(MULTI_VALUE_DELIMITER);
                            }
                            sb.append(node.getId());
                        }

                        origModel.setObject(sb.toString());
                    }

                    @Override
                    public Collection<IptcNode> getObject() {
                        String object = (String) origModel.getObject();
                        if (object != null) {
                            List<String> choices = Arrays.asList(object.split(MULTI_VALUE_DELIMITER));
                            return getProvider().toChoices(choices);
                        }

                        return null;
                    }
                });
            }
        };

        multiChoice
                .getSettings()
                .setFormatResult(
                        "function(object) { var info = object.text.split('|'); var level = info.length; var label = info[info.length - 1]; return '<span style=\"margin-left: ' + (level * 10) + 'px\">' + label + '</span>' }");
        multiChoice
                .getSettings()
                .setFormatSelection(
                        "function(object) { var info = object.text.split('|'); var label = info[info.length - 1]; var title = info.join(' > '); return '<span title=\"' + title  + '\">' + label + '</span>'; }");
        multiChoice.getSettings().setEscapeMarkup("function(m) { return m; }");
        multiChoice.setProvider(new IptcCategoryProvider());
        add(multiChoice);
    }

    public class IptcCategoryProvider extends TextChoiceProvider<IptcNode> {

        private final int PAGE_SIZE = 100;

        @Override
        protected String getDisplayText(IptcNode node) {
            IptcNode parent = node.getParent();
            StringBuilder parentsStr = new StringBuilder();
            while (parent != null) {
                parentsStr.insert(0, parent.getName() + "|");
                parent = parent.getParent();
            }
            return parentsStr.toString() + node.getName();
        }

        @Override
        protected Object getId(IptcNode node) {
            return node.getId();
        }

        @Override
        public void query(String term, int page, Response<IptcNode> response) {
            List<IptcNode> allNodes = IptcNodeProvider.getInstance().getAllNodes();

            final int offset = page * PAGE_SIZE;
            int matched = 0;
            for (IptcNode node : allNodes) {
                if (response.size() == PAGE_SIZE) {
                    break;
                }
                IptcNode parent = node;
                while (parent != null) {
                    if (parent.getName().toUpperCase().contains(term.toUpperCase())) {
                        matched++;
                        if (matched > offset) {
                            response.add(node);
                        }
                        break;
                    }
                    parent = parent.getParent();
                }
            }
            response.setHasMore(response.size() == PAGE_SIZE);
        }

        @Override
        public Collection<IptcNode> toChoices(Collection<String> ids) {
            List<IptcNode> nodes = new ArrayList<IptcNode>();
            IptcNodeProvider provider = IptcNodeProvider.getInstance();
            Collection<IptcNode> allNodes = provider.getAllNodes();
            for (IptcNode iptcNode : allNodes) {
                if (ids.contains(iptcNode.getId())) {
                    nodes.add(iptcNode);
                }
            }

            return nodes;
        }
    }
}
