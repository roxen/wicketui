package org.bitbucket.roxen.wicketui.form.nodelist;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NodeReferenceListEntryModel implements Serializable {

    private int index;
    private String identifier;

    public NodeReferenceListEntryModel(String identifier, int index) {
        this.identifier = identifier;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return "NodeReferenceListEntryModel[identifier=" + identifier + ", index=" + index + "]";
    }
}
