package org.bitbucket.roxen.wicketui.form.nodelist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.bitbucket.roxen.wicketui.behavior.draggablenodebehavior.DraggableNodeBehavior;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel;
import org.odlabs.wiquery.ui.sortable.SortableBehavior;
import org.odlabs.wiquery.ui.sortable.SortableBehavior.AjaxReceiveCallback;
import org.odlabs.wiquery.ui.sortable.SortableBehavior.AjaxUpdateCallback;
import org.odlabs.wiquery.ui.sortable.SortableHelper;

@SuppressWarnings("serial")
public class NodeReferenceListPanel extends FormComponentPanel<Void> {

    private final static Logger logger = Logger.getLogger(NodeReferenceListPanel.class.getName());

    @Inject
    private Session session;

    private final String propertyName;

    public NodeReferenceListPanel(String id, String propertyName) {
        super(id);
        this.propertyName = propertyName;
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        ArrayList<NodeReferenceListEntryModel> nodeReferenceList = new ArrayList<NodeReferenceListEntryModel>();
        final Node node = getNode();

        try {
            if (!node.hasProperty(propertyName)) {
                // DEBUG
                initFromChildNodes(node);
            }

            if (node.hasProperty(propertyName)) {
                int index = 0;
                for (Value value : node.getProperty(propertyName).getValues()) {
                    nodeReferenceList.add(new NodeReferenceListEntryModel(value.getString(), index));
                    index++;
                }
            }
        } catch (RepositoryException e) {
            logger.log(Level.WARNING, "Error getting properties", e);
        }

        ListView<NodeReferenceListEntryModel> listView = new ListView<NodeReferenceListEntryModel>("listView",
                nodeReferenceList) {
            @Override
            protected void populateItem(ListItem<NodeReferenceListEntryModel> item) {
                NodeReferenceListEntryPanel entryPanel = new NodeReferenceListEntryPanel("item", item.getModel());
                item.add(entryPanel);
                item.setOutputMarkupId(true);

                if (!isEnabledInHierarchy()) {
                    entryPanel.add(new DraggableNodeBehavior());
                }
            }
        };

        WebMarkupContainer sortableWicket = new WebMarkupContainer("sortableWicket");
        sortableWicket.add(listView);
        addOrReplace(sortableWicket);

        if (isEnabledInHierarchy()) {

            SortableBehavior sortableBehavior = new SortableBehavior();

            sortableBehavior.setReceiveEvent(new AjaxReceiveCallback() {

                @Override
                public void receive(AjaxRequestTarget target, Component source, int sortIndex, Component sortItem,
                        Component sortSender) {
                    System.out.println("never called...");
                }
            });

            sortableBehavior.setUpdateEvent(new AjaxUpdateCallback() {

                @Override
                public void update(AjaxRequestTarget target, Component source, int sortIndex, Component sortItem) {
                    System.out.println("source: " + source + ", sortIndex: " + sortIndex + ", sortItem: " + sortItem);
                    try {
                        ArrayList<Value> nodeReferenceListToUpdate = new ArrayList<Value>();

                        if (getNode().hasProperty(propertyName)) {
                            nodeReferenceListToUpdate.addAll(Arrays.asList(getNode().getProperty(propertyName)
                                    .getValues()));
                        }

                        // getNode().setProperty(propertyName, new Value[] {});
                        // session.save();
                        getNode().setProperty(propertyName, new Value[] {});
                        session.save();

                        if (sortItem == null) {
                            System.out.println(source);
                            return;
                        } else {
                            int entryIndex = ((NodeReferenceListEntryModel) sortItem.getDefaultModelObject())
                                    .getIndex();
                            if (sortIndex < entryIndex) {
                                Value elem = nodeReferenceListToUpdate.remove(entryIndex);
                                nodeReferenceListToUpdate.add(sortIndex, elem);
                            } else {
                                Value elem = nodeReferenceListToUpdate.get(entryIndex);
                                nodeReferenceListToUpdate.add(sortIndex + 1, elem);
                                nodeReferenceListToUpdate.remove(entryIndex);
                            }
                        }

                        getNode().setProperty(propertyName, nodeReferenceListToUpdate.toArray(new Value[0]));
                        session.save();
                    } catch (RepositoryException e) {
                        logger.log(Level.WARNING, "Error setting properties", e);
                    }
                }
            });

            sortableBehavior.setConnectWith(".connectedSortable");
            sortableBehavior.setHelper(new SortableHelper(SortableHelper.HelperEnum.CLONE));
            sortableBehavior.setZIndex(2);
            sortableBehavior.setAppendTo("body");
            sortableWicket.add(sortableBehavior);
        }

    }

    private void initFromChildNodes(final Node node) throws RepositoryException,
            UnsupportedRepositoryOperationException, ValueFormatException, VersionException, LockException,
            ConstraintViolationException, AccessDeniedException, ItemExistsException, ReferentialIntegrityException,
            InvalidItemStateException, NoSuchNodeTypeException {
        ArrayList<Value> nodeValues = new ArrayList<Value>();
        NodeIterator sectionNodes = node.getNodes("section");
        while (sectionNodes.hasNext()) {
            Node nextNode = sectionNodes.nextNode();
            Value nodeValue = session.getValueFactory().createValue(nextNode);
            nodeValues.add(nodeValue);
        }

        node.setProperty(propertyName, nodeValues.toArray(new Value[0]));
        session.save();
    }

    private Node getNode() {
        return ((NodeModel) getInnermostModel()).getObject();
    }
}
