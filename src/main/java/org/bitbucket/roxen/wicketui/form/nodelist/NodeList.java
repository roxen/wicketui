package org.bitbucket.roxen.wicketui.form.nodelist;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel;
import org.odlabs.wiquery.ui.sortable.SortableBehavior;
import org.odlabs.wiquery.ui.sortable.SortableBehavior.AjaxUpdateCallback;

@SuppressWarnings("serial")
public class NodeList extends FormComponentPanel<Void> {

    @Inject
    private Session session;

    public NodeList(String id) {
        super(id);
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        ArrayList<NodeReferenceListEntryModel> values = new ArrayList<NodeReferenceListEntryModel>();
        Node node = getNode();
        try {
            NodeIterator nodes = node.getNodes();
            while (nodes.hasNext()) {
                values.add(new NodeReferenceListEntryModel(nodes.nextNode().getIdentifier(), -1));
            }
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ListView<NodeReferenceListEntryModel> listView = new ListView<NodeReferenceListEntryModel>("listView", values) {
            @Override
            protected void populateItem(ListItem<NodeReferenceListEntryModel> item) {
                item.add(new NodeReferenceListEntryPanel("item", item.getModel()));
                item.setOutputMarkupId(true);
            }
        };

        WebMarkupContainer sortableWicket = new WebMarkupContainer("sortableWicket");

        SortableBehavior sortableBehavior = new SortableBehavior();
        sortableBehavior.setUpdateEvent(new AjaxUpdateCallback() {
            @Override
            public void update(AjaxRequestTarget target, Component source, int sortIndex, Component sortItem) {
                System.out.println("moved " + sortItem.getDefaultModelObjectAsString() + " to index " + sortIndex);
                // getNode().orderBefore(srcChildRelPath, destChildRelPath)

                try {
                    NodeIterator nodes = getNode().getNodes();
                    ArrayList<Value> values = new ArrayList<Value>();
                    while (nodes.hasNext()) {
                        Node nextNode = nodes.nextNode();
                        Value nodeValue = session.getValueFactory().createValue(nextNode);
                        values.add(nodeValue);
                    }

                    getNode().setProperty("refs", values.toArray(new Value[0]));
                    session.save();
                } catch (RepositoryException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        // sortableBehavior.setConnectWith(".connectedSortable");
        sortableWicket.add(sortableBehavior);

        sortableWicket.add(listView);
        addOrReplace(sortableWicket);
    }

    private Node getNode() {
        return ((NodeModel) getInnermostModel()).getObject();
    }
}
