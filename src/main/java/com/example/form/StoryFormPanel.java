package com.example.form;

import javax.jcr.Node;

import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.bitbucket.roxen.wicketui.form.iptcselect.IptcSelect;
import org.bitbucket.roxen.wicketui.panel.contenteditor.model.NodeModel.WrappingStringPropertyModel;

@SuppressWarnings("serial")
public class StoryFormPanel extends Panel {

    public StoryFormPanel(String id, IModel<?> model) {
        super(id, model);

        final TextField<String> title = new TextField<String>("title");
        add(title);

        final TextArea<String> lead = new TextArea<String>("lead") {
            @Override
            protected void onInitialize() {
                super.onInitialize();

                final WrappingStringPropertyModel model = (WrappingStringPropertyModel) getModel();
                this.setModel(new IModel<String>() {
                    private static final String OTHER_LEAD = "otherLead";

                    @Override
                    public String getObject() {
                        Node node = model.getWrappedModel().getObject();
                        try {
                            if (node.hasProperty(OTHER_LEAD)) {
                                return node.getProperty(OTHER_LEAD).getString();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return null;

                        // return model.getObject();
                    }

                    @Override
                    public void setObject(String object) {
                        Node node = model.getWrappedModel().getObject();

                        try {
                            node.setProperty(OTHER_LEAD, object);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // model.setObject(object);
                    }

                    @Override
                    public void detach() {
                        model.detach();
                    }
                });
            }
        };
        add(lead);

        final IptcSelect multiChoicePanel = new IptcSelect("iptc");
        add(multiChoicePanel);

        final TextArea<String> body = new TextArea<String>("body");
        add(body);
    }
}
