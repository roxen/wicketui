package com.example.form;

import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.bitbucket.roxen.wicketui.form.nodelist.NodeReferenceListPanel;

@SuppressWarnings("serial")
public class SectionFormPanel extends Panel {

    public SectionFormPanel(String id, IModel<?> model) {
        super(id, model);

        final TextField<String> title = new TextField<String>("title");
        add(title);

        final TextArea<String> description = new TextArea<String>("description");
        add(description);

        final NodeReferenceListPanel mainCol = new NodeReferenceListPanel("maincol", "maincol");
        add(mainCol);
    }
}
