package org.bitbucket.roxen.wicketui;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTester {

    @Test
    public void testStartPage() throws Exception {
        WebDriver driver = new FirefoxDriver();

        driver.get("http://localhost:8080/cm");

        driver.findElement(By.linkText("News")).click();
        
        WebDriverWait wait = new WebDriverWait(driver, 2, 50);
        
        for (int i = 0; i < 3; i++) {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Edit")));
            driver.findElement(By.linkText("Edit")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Save")));
            driver.findElement(By.cssSelector(".editor input[type=text]")).sendKeys("2");
            driver.findElement(By.linkText("Save")).click();
        }
        // Find the text input element by its name
        //WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        //element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the
        // element
        //element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());
    }
}
